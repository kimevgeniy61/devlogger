﻿namespace DevLoger
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.token = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.loadDevLog = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.resultID = new System.Windows.Forms.TextBox();
            this.devLogs = new System.Windows.Forms.TextBox();
            this.statusLabel = new System.Windows.Forms.Label();
            this.serverList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.logs = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Token:";
            // 
            // token
            // 
            this.token.Location = new System.Drawing.Point(66, 6);
            this.token.Name = "token";
            this.token.Size = new System.Drawing.Size(309, 20);
            this.token.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Server:";
            // 
            // loadDevLog
            // 
            this.loadDevLog.Location = new System.Drawing.Point(15, 85);
            this.loadDevLog.Name = "loadDevLog";
            this.loadDevLog.Size = new System.Drawing.Size(119, 23);
            this.loadDevLog.TabIndex = 4;
            this.loadDevLog.Text = "Load DevLog";
            this.loadDevLog.UseVisualStyleBackColor = true;
            this.loadDevLog.Click += new System.EventHandler(this.LoadDevLog_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "ResultID:";
            // 
            // resultID
            // 
            this.resultID.Location = new System.Drawing.Point(66, 57);
            this.resultID.Name = "resultID";
            this.resultID.Size = new System.Drawing.Size(309, 20);
            this.resultID.TabIndex = 6;
            // 
            // devLogs
            // 
            this.devLogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.devLogs.Location = new System.Drawing.Point(735, 32);
            this.devLogs.Multiline = true;
            this.devLogs.Name = "devLogs";
            this.devLogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.devLogs.Size = new System.Drawing.Size(648, 439);
            this.devLogs.TabIndex = 7;
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(12, 111);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(37, 13);
            this.statusLabel.TabIndex = 8;
            this.statusLabel.Text = "Status";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // serverList
            // 
            this.serverList.DisplayMember = "0";
            this.serverList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serverList.FormattingEnabled = true;
            this.serverList.Items.AddRange(new object[] {
            "https://api.eu.dasha.ai",
            "https://api.us.dasha.ai",
            "http://localhost:8080",
            "Любой возможный"});
            this.serverList.Location = new System.Drawing.Point(66, 32);
            this.serverList.Name = "serverList";
            this.serverList.Size = new System.Drawing.Size(159, 21);
            this.serverList.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(732, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Свойства/описание/DevLog";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // logs
            // 
            this.logs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logs.FormattingEnabled = true;
            this.logs.HorizontalScrollbar = true;
            this.logs.ItemHeight = 15;
            this.logs.Location = new System.Drawing.Point(399, 32);
            this.logs.Name = "logs";
            this.logs.Size = new System.Drawing.Size(330, 439);
            this.logs.TabIndex = 11;
            this.logs.SelectedIndexChanged += new System.EventHandler(this.Logs_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(396, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Краткий лог";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1401, 511);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.logs);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.serverList);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.devLogs);
            this.Controls.Add(this.resultID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.loadDevLog);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.token);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DevLogger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox token;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button loadDevLog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox resultID;
        private System.Windows.Forms.TextBox devLogs;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.ComboBox serverList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox logs;
        private System.Windows.Forms.Label label5;
    }
}

