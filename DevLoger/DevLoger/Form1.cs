﻿using System;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace DevLoger
{
    public partial class Form1 : Form
    {
        string[] ingoredMsg = { "VersionMessage", "PreparationBatchRequestMessage",
                "VersionMessage", "GraphStatusMessage", "SpeechChannelMessage",
                "IdleMessage", "StoppedPlayingAudioChannelMessage",
                "PhraseBlockMeaningSignalMessage", "FinishedPlayingAudioChannelMessage",
                "SpeechToTextInfoMessage", "VoiceSegmentRollbackMassage" };
        string[] textMsg = { "PotentialTextChannelMessage", "ConfidentTextChannelMessage", "FinalTextChannelMessage" };
        string[] sessionMsg = {"OpenSessionChannelMessage", "OpeningSessionChannelMessage",
                "OpenedSessionChannelMessage", "ClosedSessionChannelMessage",
                "CloseSessionChannelMessage"};
        string devLogResult;
        List <List <string>> headStruct;
        public Form1()
        {
            InitializeComponent();
        }
        public void ProcessJSONLog()
        {
            headStruct.Clear();
            List<string> subHead = new List<string>();
            devLogs.Text = "";
            var jsonObject = JArray.Parse(devLogResult);
            DateTime start = DateTime.ParseExact(jsonObject[0]["time"].Value<string>(), "MM/dd/yyyy HH:mm:ss", null);
            devLogs.Text += $"Start time: {start}\n";
            bool trans = false;
            foreach (var elem in jsonObject)
            {
                var time = (DateTime.ParseExact(elem["time"].Value<string>(), "MM/dd/yyyy HH:mm:ss", null) - start).Duration();
                string msg = elem["msg"]["msgId"].Value<string>();
                var incoming = elem["incoming"];
                string _from = "", _to = "";
                bool ignoredMessage = ingoredMsg.Contains(msg);
                bool potentialMessage = (msg == "RawTextChannelMessage") && 
                    elem["msg"]["textMsgId"] != null &&
                    textMsg.Contains(elem["msg"]["textMsgId"].Value<string>());
                if (msg == "NodeSwitchNotificationMessage")
                {
                    _from = elem["msg"]["from"].Value<string>();
                    _to = elem["msg"]["to"].Value<string>();
                }
                //TODO TimeOutOrRecognized
                string inc;
                if (ignoredMessage)
                {
                    continue;
                }
                if (incoming.Value<bool>())
                {
                    devLogs.Text += "I ";
                    inc = "I";
                }
                else
                {
                    devLogs.Text += "O ";
                    inc = "O";
                }
                devLogs.Text += $"{time} ";

                if (sessionMsg.Contains(msg))
                {
                    devLogs.Text += $"{msg} ";
                    subHead.Add($"{inc} {time}: {msg}");
                }
                else if (msg == "NodeSwitchNotificationMessage")
                {
                    trans = true;
                    if (elem["msg"]["transition"] != null && elem["msg"]["transition"].Value<string>() == "PopStack")
                    {
                        devLogs.Text += $"PopStack to {_to} ";
                        subHead.Add($"{inc} {time}: PopStack to {_to}");
                    }
                    else
                    {
                        devLogs.Text += $"{_from} -> {_to} ";
                        subHead.Add($"{inc} {time}: {_from} -> {_to}");
                    }
                    _from = elem["msg"]["from"].Value<string>();
                    _to = elem["msg"]["to"].Value<string>();
                }
                else if (msg == "RawTextChannelMessage")
                {
                    if (trans && (elem["msg"]["textMsgId"] != null && elem["msg"]["textMsgId"].Value<string>() != "ConfidentTextChannelMessage") || elem["msg"]["textMsgId"] == null)
                    {
                        headStruct.Add(subHead);
                        subHead = new List<string>();
                        trans = false;
                    }
                    if (elem["msg"]["textMsgId"] != null)
                    {
                        if (elem["msg"]["textMsgId"].Value<string>() == "PotentialTextChannelMessage")
                        {
                            devLogs.Text += $"Potential: {elem["msg"]["text"].Value<string>()} ";
                            subHead.Add($"{inc} {time}: Potential: {elem["msg"]["text"].Value<string>()}");
                        }
                        else if (elem["msg"]["textMsgId"].Value<string>() == "ConfidentTextChannelMessage")
                        {
                            devLogs.Text += $"Confident: {elem["msg"]["text"].Value<string>()} ";
                            subHead.Add($"{inc} {time}: Confident: {elem["msg"]["text"].Value<string>()}");
                        }
                        else if (elem["msg"]["textMsgId"].Value<string>() == "FinalTextChannelMessage")
                        {
                            devLogs.Text += $"Final: {elem["msg"]["text"].Value<string>()} ";
                            subHead.Add($"{inc} {time}: Final: {elem["msg"]["text"].Value<string>()}");
                        }
                        else
                        {
                            devLogs.Text += $"Paradox: {elem["msg"]["text"].Value<string>()} ";
                            subHead.Add($"{inc} {time}: Paradox: {elem["msg"]["text"].Value<string>()}");
                        }
                    }
                    else
                    {
                        devLogs.Text += $"DASHA: {elem["msg"]["text"].Value<string>()} ";
                        subHead.Add($"{inc} {time}: DASHA: {elem["msg"]["text"].Value<string>()}");
                    }
                }
                else if (msg == "RecognizedSpeechMessage")
                {
                    string userText = elem["msg"]["originalText"].Value<string>();
                    devLogs.Text += "\r\n\t";
                    devLogs.Text += $"User: {userText} ";
                    subHead.Add($"{inc} {time}: User: {userText}");
                    //TODO FACTS
                }
                else
                {
                    devLogs.Text += $"{msg} ";
                }
                devLogs.Text += "\r\n";
            }
            headStruct.Add(subHead);
            logs.Items.Clear();
            foreach (var elem in headStruct)
            {
                logs.Items.Add(elem[0]);
            }

        }
        private void LoadDevLog_Click(object sender, EventArgs e)
        {
            try
            {
                WebRequest req = WebRequest.Create($"{serverList.SelectedItem}/api/v1/conversations/results/{resultID.Text}/logsDev");
                req.Headers.Add("Authorization", $"Bearer {token.Text}");
                WebResponse resp = req.GetResponse();
                Stream stream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                devLogResult = sr.ReadToEnd();
                //devLogs.Text = devLogResult;
                sr.Close();
                ProcessJSONLog();
                statusLabel.Text = $"Status: Done!";
            }
            catch (WebException exp)
            {
                statusLabel.Text = $"Server error: {exp.Message}; Code: {exp.Status}";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            headStruct = new List<List<string>>();
            serverList.SelectedIndex = 0;
            try
            {
                token.Text = Environment.GetEnvironmentVariable("DASHA_SCRIPT_TOKEN");
            }
            catch {}
        }

        private void Logs_SelectedIndexChanged(object sender, EventArgs e)
        {
            devLogs.Text = "";
            int s = logs.SelectedIndex;
            for (int i = 0; i < headStruct[s].Count; ++i)
            {
                devLogs.Text += $"{headStruct[s][i]}\r\n";
            }
        }
    }
}
